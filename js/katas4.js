const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function kata1() {
    let kataResolution01 = gotCitiesCSV.split(",");
    return kataResolution01;
}

let kataResolution01DIV = document.createElement("div");
kataResolution01DIV.textContent = JSON.stringify(kata1());
document.body.appendChild(kataResolution01DIV);


function kata2() {
    let kataResolution02 = bestThing.split(" ");
    return kataResolution02;
}

let kataResolution02DIV = document.createElement("div");
kataResolution02DIV.textContent = JSON.stringify(kata2());
document.body.appendChild(kataResolution02DIV);


function kata3() {
    let kataResolution03str = gotCitiesCSV.toString();
    kataResolution03 = kataResolution03str.replace(/,/gi,";");
    return kataResolution03;
}

let kataResolution03DIV = document.createElement("div");
kataResolution03DIV.textContent = JSON.stringify(kata3());
document.body.appendChild(kataResolution03DIV);

function kata4() {
    let kataResolution04 = lotrCitiesArray.toString();
    return kataResolution04;
}

let kataResolution04DIV = document.createElement("div");
kataResolution04DIV.textContent = JSON.stringify(kata4());
document.body.appendChild(kataResolution04DIV);

function kata5() {
    let kataResolution05 = lotrCitiesArray.slice(0,5);
    return kataResolution05;
}

let kataResolution05DIV = document.createElement("div");
kataResolution05DIV.textContent = JSON.stringify(kata5());
document.body.appendChild(kataResolution05DIV);

function kata6() {
    let kataResolution06 = lotrCitiesArray.slice(3,8);
    return kataResolution06;
}

let kataResolution06DIV = document.createElement("div");
kataResolution06DIV.textContent = JSON.stringify(kata6());
document.body.appendChild(kataResolution06DIV);

function kata7() {
    let kataResolution07 = lotrCitiesArray.slice(2,5);
    return kataResolution07;
}

let kataResolution07DIV = document.createElement("div");
kataResolution07DIV.textContent = JSON.stringify(kata7());
document.body.appendChild(kataResolution07DIV);

function kata8() {
    lotrCitiesArray.splice(2,1);
    return lotrCitiesArray;
}

let kataResolution08DIV = document.createElement("div");
kataResolution08DIV.textContent = JSON.stringify(kata8());
document.body.appendChild(kataResolution08DIV);

function kata9() {
    lotrCitiesArray.splice(5,2);
    return lotrCitiesArray;
}

let kataResolution09DIV = document.createElement("div");
kataResolution09DIV.textContent = JSON.stringify(kata9());
document.body.appendChild(kataResolution09DIV);

function kata10() {
    lotrCitiesArray.splice(2,0,"Rohan");
    return lotrCitiesArray;
}

let kataResolution10DIV = document.createElement("div");
kataResolution10DIV.textContent = JSON.stringify(kata10());
document.body.appendChild(kataResolution10DIV);


function kata11() {
    lotrCitiesArray.splice(5,1,"Deadest Marshes");
    return lotrCitiesArray;
}

let kataResolution11DIV = document.createElement("div");
kataResolution11DIV.textContent = JSON.stringify(kata11());
document.body.appendChild(kataResolution11DIV);


function kata12() {
    let kataResolution12 = bestThing.slice(0,14);
    return kataResolution12;
}

let kataResolution12DIV = document.createElement("div");
kataResolution12DIV.textContent = JSON.stringify(kata12());
document.body.appendChild(kataResolution12DIV);

function kata13() {
    let kataResolution13 = bestThing.slice(-12);
    return kataResolution13;
}

let kataResolution13DIV = document.createElement("div");
kataResolution13DIV.textContent = JSON.stringify(kata13());
document.body.appendChild(kataResolution13DIV);


function kata14() {
    let kataResolution14 = bestThing.slice(23,38);
    return kataResolution14;
}

let kataResolution14DIV = document.createElement("div");
kataResolution14DIV.textContent = JSON.stringify(kata14());
document.body.appendChild(kataResolution14DIV);

function kata15() {
    let kataResolution15 = bestThing.substr(-12);
    return kataResolution15;
}

let kataResolution15DIV = document.createElement("div");
kataResolution15DIV.textContent = JSON.stringify(kata15());
document.body.appendChild(kataResolution15DIV);


function kata16() {
    let kataResolution16 = bestThing.substr(23,38-23);
    return kataResolution16;
}

let kataResolution16DIV = document.createElement("div");
kataResolution16DIV.textContent = JSON.stringify(kata16());
document.body.appendChild(kataResolution16DIV);

function kata17() {
    lotrCitiesArray.pop();
    return lotrCitiesArray;
}

let kataResolution17DIV = document.createElement("div");
kataResolution17DIV.textContent = JSON.stringify(kata17());
document.body.appendChild(kataResolution17DIV);

function kata18() {
    lotrCitiesArray.push("Deadest Marshes");
    return lotrCitiesArray;
}

let kataResolution18DIV = document.createElement("div");
kataResolution18DIV.textContent = JSON.stringify(kata18());
document.body.appendChild(kataResolution18DIV);


function kata19() {
    lotrCitiesArray.shift();
    return lotrCitiesArray;
}

let kataResolution19DIV = document.createElement("div");
kataResolution19DIV.textContent = JSON.stringify(kata19());
document.body.appendChild(kataResolution19DIV);

function kata20() {
    lotrCitiesArray.unshift("Mordor");
    return lotrCitiesArray;
}

let kataResolution20DIV = document.createElement("div");
kataResolution20DIV.textContent = JSON.stringify(kata20());
document.body.appendChild(kataResolution20DIV);
